﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerControl2 : MonoBehaviour
{

    float rotationSpeed = 180;
    [SerializeField]
    float speed;
    [Space]

    public float walkingSpeed = 8;
    public float runningSpeed = 18;
    [Space]

    public float walkingCurveSpeed = 8;
    public float runningCurveSpeed = 15;
    [Space]

    public float walkingJumpSpeed = 4;
    public float runningJumpSpeed = 8;
    [Space]

    [Range(0, 100)] public float JumpForce = 10;
    [Range(0, 100)] public float FallForce = 5.5f;

    public float jump_Rate = 0.6f;
    public float dash_Rate = 0.3f;

    float jumpCurrentRate;
    public float dashCurrentRate;

    public float deviationSpeed = 10;

    bool podePular = true;
    public bool noChao = true;

    CapsuleCollider coll;
    Animator anim;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        coll = GetComponent<CapsuleCollider>();

        speed = walkingSpeed;
        rb.drag = 2;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyCode.LeftShift) && noChao) Correr();

        if (Input.GetKey(KeyCode.LeftControl) && noChao) Agachar();

        if (Input.GetKeyUp(KeyCode.LeftControl) && noChao && podePular) Levantar();

        if (Input.GetButtonDown("Jump") && noChao && podePular) Pular();


        if (Input.GetKeyDown(KeyCode.LeftArrow) && !noChao && dashCurrentRate >= dash_Rate)
        {
            rb.AddForce(transform.right * deviationSpeed * -1);
            //anim.SetTrigger("Esquerda");
            dashCurrentRate = 0;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && !noChao && dashCurrentRate >= dash_Rate)
        {
            rb.AddForce(transform.right * deviationSpeed);
           //anim.SetTrigger("Direita");
            dashCurrentRate = 0;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) && !noChao && dashCurrentRate >= dash_Rate)
        {
            rb.AddForce(transform.forward * deviationSpeed * -1);
            //anim.SetTrigger("Tras");
            dashCurrentRate = 0;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && !noChao && dashCurrentRate >= dash_Rate)
        {
            rb.AddForce(transform.forward * deviationSpeed);
            //anim.SetTrigger("Frente");
            dashCurrentRate = 0;
        }
        if (!noChao)
        {
            jumpCurrentRate += Time.deltaTime;
            if (jumpCurrentRate >= jump_Rate) Queda();

            if (Input.GetKey(KeyCode.LeftShift)) speed = runningJumpSpeed;
            else speed = walkingJumpSpeed;
        }

        dashCurrentRate += Time.deltaTime;

        transform.Rotate(Vector3.up * Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime);

        Animator();
    }

    void FixedUpdate()
    {
        Vector3 direction = Vector3.zero;

        direction = transform.forward * Input.GetAxis("Vertical");


        if (Mathf.Abs(Input.GetAxis("Horizontal")) == 1 && noChao)
        {
            if (Input.GetKey(KeyCode.LeftShift)) speed = runningCurveSpeed;
            else speed = walkingCurveSpeed;
        }
        else
        {
            if (Input.GetKey(KeyCode.LeftShift) && noChao) speed = runningSpeed;
            else speed = walkingSpeed;
        }


        if (!Input.GetKeyDown(KeyCode.Space))
            rb.velocity += direction * speed * Time.deltaTime;
    }

    void Correr()
    {
        speed = runningSpeed;
    }

    void Pular()
    {
        rb.velocity = new Vector3(rb.velocity.x, JumpForce, rb.velocity.z);
        anim.SetTrigger("Pular");
        noChao = false;
    }
    void Queda()
    {
        rb.velocity = new Vector3(rb.velocity.x, -FallForce, rb.velocity.z);
        jumpCurrentRate = 0;
        noChao = true;
    }

    void Agachar()
    {
        coll.center = new Vector3(0, 0.49f, 0);
        coll.height = 0.96f;
    }
    void Levantar()
    {
        coll.center = new Vector3(0, 0.7f, 0);
        coll.height = 1.38f;
    }

    void Animator()
    {
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
        anim.SetFloat("Vertical", Input.GetAxis("Vertical"));

        anim.SetBool("Correndo", Input.GetKey(KeyCode.LeftShift));
        if (noChao) anim.SetBool("Agachado", Input.GetKey(KeyCode.LeftControl));
    }

    void OnTriggerStay(Collider other)
    {
        podePular = true;
    }

    void OnTriggerExit(Collider other)
    {
        podePular = false;
    }
}



