﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class rotateUI : MonoBehaviour
{
    public enum Rotate
    {
        X,
        Y,
        Z,
        Nenhuma
    }
    public Rotate rotacionar;
    public enum Sentido
    {
        Horario,
        AntiHorario,
        Nenhum
    }
    public Sentido sentidoRotacao;


    public float valorTransicao;
    public float segFadeIn;
    public float segHold;
    public string state = "FadeIn";
    float contBlackOut;

    private void OnEnable()
    {
        switch (rotacionar)
        {
            case Rotate.X:
                StartCoroutine(RotateX(segFadeIn, segHold, valorTransicao, transform.localRotation.x));
                break;

            case Rotate.Y:
                StartCoroutine(RotateY(segFadeIn, segHold, valorTransicao, transform.localRotation.y));
                break;

            case Rotate.Z:
                StartCoroutine(RotateZ(segFadeIn, segHold, valorTransicao, transform.localRotation.z));
                break;


            default:
                break;
        }
    }

    IEnumerator RotateX(float fadeIn, float hold, float transicao, float original)
    {
        while (enabled)
        {
            switch (state)
            {
                case "FadeIn":
                    valorTransicao += Mathf.Lerp(0, 1, 1 / (segFadeIn / 0.01f));
                    if (sentidoRotacao == Sentido.AntiHorario)
                        transform.localRotation = Quaternion.Euler(new Vector3(Mathf.Lerp(original, original + 360f, valorTransicao), transform.localRotation.y, transform.localRotation.z));
                    if (sentidoRotacao == Sentido.Horario)
                        transform.localRotation = Quaternion.Euler(new Vector3(Mathf.Lerp(original, original - 360f, valorTransicao), transform.localRotation.y, transform.localRotation.z));

                    if (valorTransicao >= 1)
                    {
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(original + 360f, transform.localRotation.y, transform.localRotation.z));
                        state = "BlackOut";
                    }
                    break;

                case "BlackOut":
                    if (contBlackOut < (segHold / 0.01f))
                    {
                        contBlackOut++;
                    }
                    else
                    {
                        contBlackOut = 0;
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(original, transform.localRotation.y, transform.localRotation.z));
                        state = "FadeIn";
                    }
                    break;

                default:
                    break;
            }

            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator RotateY(float fadeIn, float hold, float transicao, float original)
    {
        while (enabled)
        {
            switch (state)
            {
                case "FadeIn":
                    valorTransicao += Mathf.Lerp(0, 1, 1 / (segFadeIn / 0.01f));
                    if (sentidoRotacao == Sentido.AntiHorario)
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, Mathf.Lerp(original, original + 360f, valorTransicao), transform.localRotation.z));
                    if (sentidoRotacao == Sentido.Horario)
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, Mathf.Lerp(original, original - 360f, valorTransicao), transform.localRotation.z));

                    if (valorTransicao >= 1)
                    {
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, original + 360f, transform.localRotation.z));
                        state = "BlackOut";
                    }
                    break;

                case "BlackOut":
                    if (contBlackOut < (segHold / 0.01f))
                    {
                        contBlackOut++;
                    }
                    else
                    {
                        contBlackOut = 0;
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, original, transform.localRotation.z));
                        state = "FadeIn";
                    }
                    break;

                default:
                    break;
            }

            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator RotateZ(float fadeIn, float hold, float transicao, float original)
    {
        while (enabled)
        {
            switch (state)
            {
                case "FadeIn":
                    valorTransicao += Mathf.Lerp(0, 1, 1 / (segFadeIn / 0.01f));

                    if (sentidoRotacao == Sentido.AntiHorario)
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, transform.localRotation.y, Mathf.Lerp(original, original + 360f, valorTransicao)));
                    if (sentidoRotacao == Sentido.Horario)
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, transform.localRotation.y, Mathf.Lerp(original, original - 360f, valorTransicao)));

                    if (valorTransicao >= 1)
                    {
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, transform.localRotation.y, original + 360f));
                        state = "BlackOut";
                    }
                    break;

                case "BlackOut":
                    if (contBlackOut < (segHold / 0.01f))
                    {
                        contBlackOut++;
                    }
                    else
                    {
                        contBlackOut = 0;
                        valorTransicao = 0;
                        transform.localRotation = Quaternion.Euler(new Vector3(transform.localRotation.x, transform.localRotation.y, original));
                        state = "FadeIn";
                    }
                    break;

                default:
                    break;
            }

            yield return new WaitForSeconds(0.01f);
        }
    }
}
