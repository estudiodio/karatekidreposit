﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AtributoCanvas
{
    public string nome;
    public Image fillTalisma;
    public Image fill;
}
public class Inventario : MonoBehaviour
{
    [SerializeField] List<ArmaNome> armasInventario = new List<ArmaNome>();
    [SerializeField] List<TalismaNome> talismasInventario = new List<TalismaNome>();
    [Header("Player")]
    public AtributoCanvas[] atributosPlayer;//forca=0, agilidade=1
    [Header("Arma")]
    public AtributoCanvas[] atributosArma; //velocidade=0, dano=1, alcance =2
    public Image armaImage;
    public Text armaText;
    [Header("Talisma")]
    public AtributoCanvas[] atributosTalisma;//forca=0, agilidade=1, velocidade=2, dano=3, alcance =4
    public Image talismaImage;
    public Text talismaText;

    public Image faixa;

    public void PauseSwitchEnable()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    public void PauseEnable(bool enabled)
    {
        gameObject.SetActive(enabled);
    }

    public void UpdatePlayer()
    {
        PlayerController playerNovo = PlayerController.Instance;

        if (atributosPlayer[0] != null) atributosPlayer[0].fill.fillAmount = (float)playerNovo.forca / 10f;
        if (atributosPlayer[1] != null) atributosPlayer[1].fill.fillAmount = (float)playerNovo.agilidade / 10f;

        //forca=0, agilidade=1
    }
    public void UpdateArma()
    {
        ArmaNome armaNova = PlayerController.Instance.arma;
        Arma armaNovaVal = PlayerController.Instance.armaValores;

        //imagem
        Sprite armaImagem = Combate.Instance.ArmaSprite(armaNova);
        if (armaImage && armaImagem) armaImage.sprite = armaImagem;
        else if (armaImage) armaImage.enabled = false;

        //nome
        string armaNome = Combate.Instance.ArmaNome(armaNova);
        if (armaText) armaText.text = armaNome;

        if (atributosArma[0] != null) atributosArma[0].fill.fillAmount = (float)armaNovaVal.velocidade / 10f;
        if (atributosArma[1] != null) atributosArma[1].fill.fillAmount = (float)armaNovaVal.dano / 10f;
        if (atributosArma[2] != null) atributosArma[2].fill.fillAmount = (float)armaNovaVal.alcance / 10f;

        //velocidade=0, dano=1, alcance =2
    }
    public void UpdateTalisma()
    {
        TalismaNome talismaNovo = PlayerController.Instance.talisma;
        Talisma talismaNovoVal = PlayerController.Instance.talismaValores;

        //imagem
        Sprite talismaImagem = Combate.Instance.TalismaSprite(talismaNovo);
        if (talismaImage && talismaImagem) talismaImage.sprite = talismaImagem;
        else if (talismaImage) talismaImage.enabled = false;

        //nome
        string talismaNome = Combate.Instance.TalismaNome(talismaNovo);
        if(talismaText) talismaText.text = talismaNome;

        if (atributosTalisma[0] != null) atributosTalisma[0].fill.fillAmount = (float)talismaNovoVal.forca / 10f;
        if (atributosTalisma[1] != null) atributosTalisma[1].fill.fillAmount = (float)talismaNovoVal.agilidade / 10f;
        if (atributosTalisma[2] != null) atributosTalisma[2].fill.fillAmount = (float)talismaNovoVal.velocidade / 10f;
        if (atributosTalisma[3] != null) atributosTalisma[3].fill.fillAmount = (float)talismaNovoVal.dano / 10f;
        if (atributosTalisma[4] != null) atributosTalisma[4].fill.fillAmount = (float)talismaNovoVal.alcance / 10f;

        if (atributosTalisma[0] != null) atributosTalisma[0].fillTalisma.fillAmount = atributosPlayer[0].fill.fillAmount + (float)talismaNovoVal.forca / 10f;
        if (atributosTalisma[1] != null) atributosTalisma[1].fillTalisma.fillAmount = atributosPlayer[1].fill.fillAmount + (float)talismaNovoVal.agilidade / 10f;
        if (atributosTalisma[2] != null) atributosTalisma[2].fillTalisma.fillAmount = atributosArma[0].fill.fillAmount + (float)talismaNovoVal.velocidade / 10f;
        if (atributosTalisma[3] != null) atributosTalisma[3].fillTalisma.fillAmount = atributosArma[1].fill.fillAmount + (float)talismaNovoVal.dano / 10f;
        if (atributosTalisma[4] != null) atributosTalisma[4].fillTalisma.fillAmount = atributosArma[2].fill.fillAmount + (float)talismaNovoVal.alcance / 10f;

        //forca=0, agilidade=1, velocidade=2, dano=3, alcance =4
    }
    public void UpdateFaixa()
    {
        if (faixa) faixa.color = PlayerController.Instance.faixaValores.corFaixa;
    }

    void AdicionarArma(ArmaNome novaArma)
    {
        armasInventario.Add(novaArma);
    }
    void AdicionarTalisma(TalismaNome novoTalisma)
    {
        talismasInventario.Add(novoTalisma);
    }
}
