﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmaCanvas : MonoBehaviour
{
    public Text nome;
    public Image imagem;
    public int velocidade;
    public int velocidadeBonus;
    public int dano;
    public int danoBonus;
    public int alcance;
    public int alcanceBonus;
}

/*
public class Arma
{
    public string nome;
    public string nomeReal;
    public int ID;
    public int velocidade;
    public int dano;
    public int alcance;
    public Sprite imagem;
}*/

/*public class Talisma
{
    public string nome;
    public string nomeReal;

    [Header("Atributos Combatente")]
    public int forca;
    public int agilidade;

    [Header("Atributos Arma")]
    public int velocidade;
    public int dano;
    public int alcance;
    public Sprite imagem;
}*/
