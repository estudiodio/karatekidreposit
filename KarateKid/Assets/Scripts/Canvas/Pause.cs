﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public Image armaImage;
    public Text armaText;

    public Image talismaImage;
    public Text talismaText;

    public Image faixa;

    public void PauseSwitchEnable()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
    public void PauseEnable(bool enabled)
    {
        gameObject.SetActive(enabled);
    }
    public void UpdateArma()
    {
        ArmaNome armaNova = PlayerController.Instance.arma;
        Arma armaNovaVal = PlayerController.Instance.armaValores;

        //imagem
        Sprite armaImagem = Combate.Instance.ArmaSprite(armaNova);
        if (armaImage && armaImagem) armaImage.sprite = armaImagem;
        else if (armaImage) armaImage.enabled = false;

        //nome
        string armaNome = Combate.Instance.ArmaNome(armaNova);
        if (armaText) armaText.text = armaNome;
    }
    public void UpdateTalisma()
    {
        TalismaNome talismaNovo = PlayerController.Instance.talisma;
        Talisma talismaNovoVal = PlayerController.Instance.talismaValores;

        //imagem
        Sprite talismaImagem = Combate.Instance.TalismaSprite(talismaNovo);
        if (talismaImage && talismaImagem) talismaImage.sprite = talismaImagem;
        else if (talismaImage) talismaImage.enabled = false;

        //nome
        string talismaNome = Combate.Instance.TalismaNome(talismaNovo);
        if (talismaText) talismaText.text = talismaNome;

    }
    public void UpdateFaixa()
    {
        if (faixa) faixa.color = PlayerController.Instance.faixaValores.corFaixa;
    }

}
