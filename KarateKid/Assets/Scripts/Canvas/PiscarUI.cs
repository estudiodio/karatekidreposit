﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public enum Actions
{
    Nothing,
    Disable
}

public class PiscarUI : MonoBehaviour
{
    public enum PiscarInterface
    {
        Nenhuma,
        Imagem,
        RawImage,
        Outline,
        Text
    }
    public PiscarInterface piscar;
    public Call callOn;
    public Color corInterpolar;

    float valorTransicao;
    public float segFadeIn;
    public float segHold;
    public float segFadeOut;
    string state = "FadeIn";
    public bool useRealTime;
    public bool stopOnHalf = false;
    public bool stopOnFull = false;
    float contBlackOut;

    private void Awake()
    {
        if (callOn == Call.OnAwake)
            Iniciar();
    }

    private void OnEnable()
    {
        if (callOn == Call.OnEnable)
            Iniciar();
    }
    private void Start()
    {
        if (callOn == Call.OnStart)
            Iniciar();
    }

    public void Iniciar()
    {
        switch (piscar)
        {
            case PiscarInterface.Imagem:
                StartCoroutine(ColorLerp(segFadeIn, segHold, segFadeOut, valorTransicao, GetComponent<Image>(), null, null, null, GetComponent<Image>().color));
                break;

            case PiscarInterface.RawImage:
                StartCoroutine(ColorLerp(segFadeIn, segHold, segFadeOut, valorTransicao, null, GetComponent<RawImage>(), null, null, GetComponent<RawImage>().color));
                break;

            case PiscarInterface.Outline:
                StartCoroutine(ColorLerp(segFadeIn, segHold, segFadeOut, valorTransicao, null, null, GetComponent<Outline>(), null, GetComponent<Outline>().effectColor));
                break;

            case PiscarInterface.Text:
                StartCoroutine(ColorLerp(segFadeIn, segHold, segFadeOut, valorTransicao, null, null, null, GetComponent<Text>(), GetComponent<Text>().color));
                break;

            default:
                break;
        }
    }

    IEnumerator ColorLerp(float fadeIn, float hold, float fadeOut, float transicao, Image image, RawImage rawimage, Outline outline, Text text, Color original)
    {
        while (enabled)
        {
            switch (state)
            {
                case "FadeIn":
                    valorTransicao += Mathf.Lerp(0, 1, 1 / (segFadeIn / 0.01f));
                    if (image) image.color = Color.Lerp(original, corInterpolar, valorTransicao);
                    if (rawimage) rawimage.color = Color.Lerp(original, corInterpolar, valorTransicao);
                    if (outline) outline.effectColor = Color.Lerp(original, corInterpolar, valorTransicao);
                    if (text) text.color = Color.Lerp(original, corInterpolar, valorTransicao);

                    if (valorTransicao >= 1)
                    {
                        valorTransicao = 0;

                        if (image) image.color = corInterpolar;
                        if (rawimage) rawimage.color = corInterpolar;
                        if (outline) outline.effectColor = corInterpolar;
                        if (text) text.color = corInterpolar;

                        if (!stopOnHalf)
                        state = "BlackOut";
                        else yield return null;
                    }
                    break;

                case "BlackOut":
                    if (segHold > 99)
                        break;
                    if (contBlackOut < (segHold / 0.01f))
                    {
                        contBlackOut++;
                    }
                    else
                    {
                        contBlackOut = 0;
                        state = "FadeOut";
                    }
                    break;

                case "FadeOut":
                    valorTransicao += Mathf.Lerp(0, 1, 1 / (segFadeOut / 0.01f));
                    if (image) image.color = Color.Lerp(corInterpolar, original, valorTransicao);
                    if (rawimage) rawimage.color = Color.Lerp(corInterpolar, original, valorTransicao);
                    if (outline) outline.effectColor = Color.Lerp(corInterpolar, original, valorTransicao);
                    if (text) text.color = Color.Lerp(corInterpolar, original, valorTransicao);

                    if (valorTransicao >= 1)
                    {
                        valorTransicao = 0;

                        if (image) image.color = original;
                        if (rawimage) rawimage.color = original;
                        if (outline) outline.effectColor = original;
                        if (text) text.color = original;

                        if (!stopOnFull)
                            state = "FadeIn";
                        else yield return null;
                    }

                    break;

                default:
                    break;
            }

            if (!useRealTime)
                yield return new WaitForSeconds(0.01f);
            else
                yield return StartCoroutine(CoroutineUtil.WaitForRealSeconds(0.01f));
        }
    }

    public static class CoroutineUtil
    {
        public static IEnumerator WaitForRealSeconds(float time)
        {
            float start = Time.realtimeSinceStartup;
            while (Time.realtimeSinceStartup < start + time)
            {
                yield return null;
            }
        }
    }

}
