﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnMouseOverGUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public UnityEvent onMouseOver;
    public UnityEvent onMouseExit;
    public void OnPointerEnter(PointerEventData eventData)
    {
        onMouseOver.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        onMouseExit.Invoke();
    }
}
