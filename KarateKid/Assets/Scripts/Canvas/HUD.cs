﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    public static HUD Instance;
    public Image[] imgsCooldownSkills;
    public Image vidaVigorImgFill;
    public Image vidaImgFill;
    PlayerController player;

    void Start()
    {
        Instance = this;
        player = PlayerController.Instance;
        player.imgsCooldownSkills = imgsCooldownSkills;
        player.vidaImgFill = vidaImgFill;
        player.vidaVigorImgFill = vidaVigorImgFill;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
