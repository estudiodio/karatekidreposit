﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager Instance;
    public HUD hud;
    public Inventario inventario;
    public Pause pause;
    public GameObject morte;

    void Start()
    {
        Instance = this;
    }
}
