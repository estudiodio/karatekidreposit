﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneManagement : MonoBehaviour
{
    public void LoadScene(int num)
    {
        SceneManager.LoadScene(num);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
    public void TimeScale(int time)
    {
        Time.timeScale = time;
    }
}
