using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

//punho, bastao, lanca, garras, faca
//SONHO: arco, lan�a, tubaraoooo (armas longo alcan�e) SONHO: dormir

[System.Serializable]
public class TipoArma
{
    [Tooltip("Tipo da Arma / Classe")]
    public string nome;
    [Tooltip("Todas as armas que fazem parte dessa classe")]
    public Arma[] armas;
    [Tooltip("Combos que essa arma apresenta")]
    public GolpeCombo[] golpeCombo;
    [Tooltip("Skills/Golpes especiais com cooldown")]
    public GolpeSkill[] golpeSkills;
}//pra cada tipo de arma tem um animacao diferente

[System.Serializable]
public class Arma
{
    [Tooltip("Nome n�o pode ter espa�os ou caracteres especiais, para Enums")]
    public string nome;
    [Tooltip("Nome real para aparecer no jogo")]
    public string nomeReal;
    [HideInInspector] public int ID;// { get { return ID; } set { ID = value; } }
    [Range(0, 10)]
    public int velocidade;
    [Range(0, 10)]
    public int dano;
    [Range(0, 10)]
    public int alcance;
    public Sprite imagem;
}// esperando desenvolver do jogo pra balancear a velocidade (das animacoes) e alcance (tamanho dos colisores), dano j� sendo usado

[System.Serializable]
public class Faixa
{
    public string nome;
    [HideInInspector] public int ID;// { get { return ID; } set { ID = value; } }
    public int xp;
    public Color corFaixa;
} // esperando desenvolver do jogo pra balancear o ganho de xp e evolu��o das faixas

[System.Serializable]
public class Talisma // Combatente:  int forca, int agilidade,   Arma: int velocidade, int dano, int alcance
{
    [Tooltip("Nome n�o pode ter espa�os ou caracteres especiais, para Enums")]
    public string nome;
    [Tooltip("Nome real para aparecer no jogo")]
    public string nomeReal;
    [HideInInspector] public int ID;// { get { return ID; } set { ID = value; } }

    [Header("Atributos Combatente")]
    [Range(0, 10)]
    public int forca;
    [Range(0, 10)]
    public int agilidade;

    [Header("Atributos Arma")]
    [Range(0, 10)]
    public int velocidade;
    [Range(0, 10)]
    public int dano;
    [Range(0, 10)]
    public int alcance;
    public Sprite imagem;
}

[System.Serializable]
public class Golpe
{
    public string nome;
    [Range(0, 100)]
    public int danoMin, danoMax;
}

[System.Serializable]
public class GolpeSkill
{
    public string nome;
    [Range(0, 100)]
    public float cooldown;
    public Golpe golpe;
}

[System.Serializable]
public class GolpeCombo
{
    public string nome;
    public Golpe[] sequenciaGolpes;
}

public class Combate : MonoBehaviour
{
    public static Combate Instance;

    [Header("FAIXAS")]
    public Faixa[] faixas;
    [Header("TALISMAS")]
    public Talisma[] talismas;
    [Header("ARMAS")]
    public TipoArma[] tipoArmas;
    [Space(20)]
    [Header("Atualizar")]

    public Dictionary<int, TipoArma> tipoArmasDicionario;
    public Dictionary<int, Arma> armasDicionario;
    public Dictionary<int, Faixa> faixasDicionario;
    public Dictionary<int, Talisma> talismaDicionario;

    public void Preencher()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);

        #region Armas
        // PREENCHER ARMAS
        tipoArmasDicionario = new Dictionary<int, TipoArma>();
        tipoArmasDicionario.Clear();

        armasDicionario = new Dictionary<int, Arma>();
        armasDicionario.Clear();

        int ID = 0;
        List<string> listaArmas = new List<string>();
        for (int i = 0; i < tipoArmas.Length; i++)
        {
            TipoArma tipo = tipoArmas[i];
            tipoArmasDicionario.Add(i, tipoArmas[i]);

            for (int f = 0; f < tipo.armas.Length; f++)
            {
                tipo.armas[f].ID = ID;
                armasDicionario.Add(ID, tipo.armas[f]);
                listaArmas.Add(tipo.armas[f].nome);
                ID++;
            }
        }
        #endregion

        #region Faixas
        // PREENCHER FAIXAS
        faixasDicionario = new Dictionary<int, Faixa>();
        faixasDicionario.Clear();
        List<string> listaFaixas = new List<string>();
        for (int i = 0; i < faixas.Length; i++)
        {
            faixas[i].ID = i;
            faixasDicionario.Add(i, faixas[i]);
            listaFaixas.Add(faixas[i].nome);
        }
        #endregion

        #region Talismas
        // PREENCHER FAIXAS
        talismaDicionario = new Dictionary<int, Talisma>();
        talismaDicionario.Clear();
        List<string> listaTalisma = new List<string>();
        for (int i = 0; i < talismas.Length; i++)
        {
            talismas[i].ID = i;
            talismaDicionario.Add(i, talismas[i]);
            listaTalisma.Add(talismas[i].nome);
        }
        #endregion

        GenerateEnum.Generate(listaArmas, "ArmaNome");
        GenerateEnum.Generate(listaFaixas, "FaixaNome");
        GenerateEnum.Generate(listaTalisma, "TalismaNome");
        GenerateColliders.Generate(listaArmas, "ArmaColliders");
    }

    public void PrintarArmas()
    {
        if (tipoArmasDicionario == null)
        {
            Debug.LogError("O DICIONARIO EST� VAZIO. ATUALIZE-O");
            return;
        }
        Debug.Log("TOTAL DE " + armasDicionario.Count + " ARMAS");

        for (int i = 0; i < tipoArmasDicionario.Count; i++)
        {
            var tipo = tipoArmasDicionario[i];
            Debug.Log("[" + i + "] " + tipo.nome);

            for (int j = 0; j < tipo.armas.Length; j++)
            {
                var arma = tipo.armas[j];
                Debug.Log("[" + tipo.nome + "] " + arma.nome + " [ID " + arma.ID + "]");
            }
        }
    }

    public void Start()
    {
        Preencher();
    }

    public void PlayerSortearCombo()
    {
        PlayerController.Instance.SortearCombo();
    }
    public void PlayerSortearSkill()
    {
        PlayerController.Instance.SortearSkill();
    }

    public Sprite ArmaSprite(ArmaNome arma)
    {
        return armasDicionario[(int)arma].imagem;
    }
    public Sprite ArmaSprite(int arma)
    {
        return armasDicionario[arma].imagem;
    }
    public string ArmaNome(ArmaNome arma)
    {
        return armasDicionario[(int)arma].nomeReal;
    }
    public string ArmaName(int arma)
    {
        return armasDicionario[arma].nomeReal;
    }

    public Sprite TalismaSprite(TalismaNome talisma)
    {
        return talismaDicionario[(int)talisma].imagem;
    }
    public Sprite TalismaSprite(int talisma)
    {
        return talismaDicionario[talisma].imagem;
    }
    public string TalismaNome(TalismaNome talisma)
    {
        return talismaDicionario[(int)talisma].nomeReal;
    }
    public string TalismaName(int talisma)
    {
        return talismaDicionario[talisma].nomeReal;
    }
}

public class GenerateEnum
{
    static List<string> listaBase;
    static string nomeEnumBase;
    public static void Generate(List<string> lista, string nomeEnum)
    {
        listaBase = lista;
        nomeEnumBase = nomeEnum;
        GenerateEnumNow();
    }

    [MenuItem("Tools/GenerateEnum")]
    public static void GenerateEnumNow()
    {
        string enumName = nomeEnumBase;
        string[] enumEntries = listaBase.ToArray();
        string filePathAndName = "Assets/Scripts/Combate/" + enumName + ".cs"; //The folder Assets/Scripts/Combate/ is expected to exist

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine("public enum " + enumName);
            streamWriter.WriteLine("{");
            for (int i = 0; i < enumEntries.Length; i++)
            {
                streamWriter.Write("\t" + enumEntries[i].Replace(" ", string.Empty));

                if (i < enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
                else
                    streamWriter.WriteLine("\t");
            }
            streamWriter.WriteLine("}");
        }
        AssetDatabase.Refresh();
    }
}

public class GenerateColliders
{
    static List<string> listaBase;
    static string nomeEnumBase;

    public static void Generate(List<string> lista, string nomeClass)
    {
        listaBase = lista;
        nomeEnumBase = nomeClass;
        GenerateCollidersNow();
    }

    [MenuItem("Tools/GenerateCollidersArmas")]
    public static void GenerateCollidersNow()
    {
        string className = nomeEnumBase;
        string[] arrayArmas = listaBase.ToArray();
        string filePathAndName = "Assets/Scripts/Combate/" + className + ".cs"; //Assets/Scripts/Combate/ is expected to exist

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine("using UnityEngine;");
            streamWriter.WriteLine("");

            streamWriter.WriteLine("[System.Serializable]");
            streamWriter.WriteLine("public class ColliderArma");
            streamWriter.WriteLine("{");
            streamWriter.WriteLine("public string nome;");
            streamWriter.WriteLine("public Collider[] collidersArma;");
            streamWriter.WriteLine("}");
            streamWriter.WriteLine("");


            streamWriter.WriteLine("[System.Serializable]");
            streamWriter.WriteLine("public class " + className);
            streamWriter.WriteLine(" { ");
            streamWriter.WriteLine(" [SerializeField] public ColliderArma[] colliders; ");
            streamWriter.WriteLine(" } ");


            streamWriter.WriteLine("[System.Serializable]");
            streamWriter.WriteLine("public class " + className + "Values");
            streamWriter.WriteLine("{");
            streamWriter.WriteLine("");

            for (int i = 0; i < arrayArmas.Length; i++)
            {
                streamWriter.WriteLine("public static ColliderArma " + arrayArmas[i].Replace(" ", string.Empty) +
                    " = new ColliderArma {nome = " + "\"" + arrayArmas[i].Replace(" ", string.Empty) + "\"" + ", collidersArma = new Collider[0]};");
            }
            streamWriter.WriteLine("");

            string stringVariavel = "[SerializeField] public static ColliderArma[] colliders = new ColliderArma[" + arrayArmas.Length + "]";

            stringVariavel += " { ";
            for (int i = 0; i < arrayArmas.Length; i++)
            {
                stringVariavel += arrayArmas[i].Replace(" ", string.Empty);
                if (i != arrayArmas.Length - 1) //nao acabou final
                    stringVariavel += ", ";
            }
            stringVariavel += " };";

            streamWriter.WriteLine(stringVariavel);
            streamWriter.WriteLine("");

            streamWriter.WriteLine("}");
        }
        AssetDatabase.Refresh();


    }
}

[CustomEditor(typeof(Combate))]
public class ObjectBuilderEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Combate combate = (Combate)target;

        GUILayout.Space(30);
        GUILayout.Label("ATUALIZAR DICIONARIO", EditorStyles.boldLabel);
        GUILayout.Space(5);

        if (GUILayout.Button("ATUALIZAR"))
        {
            combate.Preencher();
            Debug.Log("DICION�RIO ATUALIZADO");
        }

        GUILayout.Space(30);
        GUILayout.Label("VER TODAS ARMAS E IDS", EditorStyles.boldLabel);
        GUILayout.Space(5);

        if (GUILayout.Button("DEBUG ARMAS"))
        {
            combate.PrintarArmas();
        }
    }
}