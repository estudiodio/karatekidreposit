using UnityEngine;

[System.Serializable]
public class ColliderArma
{
public string nome;
public Collider[] collidersArma;
}

[System.Serializable]
public class ArmaColliders
 { 
 [SerializeField] public ColliderArma[] colliders; 
 } 
[System.Serializable]
public class ArmaCollidersValues
{

public static ColliderArma MaoNormal = new ColliderArma {nome = "MaoNormal", collidersArma = new Collider[0]};
public static ColliderArma MaocomLuva = new ColliderArma {nome = "MaocomLuva", collidersArma = new Collider[0]};
public static ColliderArma BastaoGrande = new ColliderArma {nome = "BastaoGrande", collidersArma = new Collider[0]};
public static ColliderArma BastaoPequeno = new ColliderArma {nome = "BastaoPequeno", collidersArma = new Collider[0]};

[SerializeField] public static ColliderArma[] colliders = new ColliderArma[4] { MaoNormal, MaocomLuva, BastaoGrande, BastaoPequeno };

}
