﻿using UnityEngine;
using UnityEngine.Events;

public class OnMouseOverCollider : MonoBehaviour
{
    public UnityEvent onMouseOver;
    public UnityEvent onMouseExit;
    void OnMouseOver()
    {
        onMouseOver.Invoke();
    }

    void OnMouseExit()
    {
        onMouseExit.Invoke();
    }
}
