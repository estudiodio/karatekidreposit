﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class Atividade
{
    public string nome;
    public int progresso;
    public bool realizando;
    public Collider areaAtividade;
    public Transform[] posicoes;
}

public class Atividades : MonoBehaviour
{
    public Atividade[] atividades;
    /*
     * treinoForça
     * treinoAgilidade
     * treinoDupla //pessoas se batendo
     * treinoResiliencia ???
     * horaRefeiçao
     * horaPatio
     * horaQuarto
     */

    public Dictionary<string, Atividade> atividadesDicionario = new Dictionary<string, Atividade>();

    public void PreencherDicionario()
    {
        for (int i = 0; i < atividades.Length; i++)
        {
            atividadesDicionario.Add(atividades[i].nome, atividades[i]);
        }
    }

    /*void OnAwake()
    {
        PreencherDicionario();
    }*/
}
