﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

#if UNITY_EDITOR
using UnityEditor.Animations;
#endif

public class Destrutivel : MonoBehaviour //recebe pauladas, mas não dá pauladas. Esse seria o combatente.
{
    [SerializeField] protected int vida = 100;
    [SerializeField] protected int vidaMax = 100;
    [SerializeField] protected int vidaVigor = 100;
    [Range(1,5)]
    [SerializeField] public float velocidadeRecuperarVidaVigor = 1;
    float cooldownRecover = 0;
    public Image vidaVigorImgFill;
    public Image vidaImgFill;
    [SerializeField] protected bool imortal = false;
    protected bool vivo = true;
    [SerializeField] static int numAnimDano;

    public UnityEvent morte;
    protected Animator anim;

    public int Vida()
    {
        return vida;
    }

    public int VidaMax()
    {
        return vidaMax;
    }

    public void ReceberDano(int dano)
    {
        if (imortal) return;
        if (!vivo) return;

        if (vida != vidaVigor)
            vidaVigor = vida;

        vida = Mathf.Clamp(vida -= dano, 0, 100);

        cooldownRecover = 0;

        if (anim)
            anim.SetTrigger("tomarDano");


        AtualizarCanvas();

        if (vida <= 0)
        {
            if (anim)
                anim.SetBool("morto", true);
            Morrer();
        }
    }

    public void SortearAnimacaoDano(int possibilidades) //chamando no animator
    {
        numAnimDano = Random.Range(0, possibilidades - 1);
        if (gameObject.activeSelf)
            GetComponent<Animator>().SetInteger("tomouDano", numAnimDano);
    }

    public void RecuperarVida(int vidaRecuperar)
    {
        if ((vida + vidaRecuperar) <= vidaVigor)
            vida += (vidaRecuperar);
        else
            vida = vidaVigor;

        AtualizarCanvas();
    } //clampar a vida pra nao subir mais do que o vigor

    void Morrer()
    {
        GetComponent<Animator>().SetBool("morto", true);
        morte.Invoke();
    }

    public void DestroyAutoSecs(int secs)
    {
        StartCoroutine(AutoDestroy(secs));
    }

    IEnumerator AutoDestroy(int secs)
    {
        yield return new WaitForSeconds(secs);
        Destroy(gameObject);
    }
    private void FixedUpdate()
    {
        RecuperarVida();
    }

    protected void RecuperarVida()
    {
        if (vida < vidaVigor)
        {
            cooldownRecover += (0.02f * velocidadeRecuperarVidaVigor);
            if (cooldownRecover >= 1)
            {
                RecuperarVida(1);
                cooldownRecover = 0;
            }
        }
    }
    void AtualizarCanvas()
    {
        if (vidaImgFill) vidaImgFill.fillAmount = (float)vida / (float)vidaMax;
        if (vidaVigorImgFill) vidaVigorImgFill.fillAmount = (float)vidaVigor / (float)vidaMax;
    }
}

public enum estadoCombate { idle, ataque, esquiva }


[RequireComponent(typeof(Animator))]
public class Combatente : Destrutivel   //recebe e da pauladas
{
    public estadoCombate estado;
    public ArmaNome arma;
    public Arma armaValores;
    public TalismaNome talisma;
    public Talisma talismaValores;
    public ArmaColliders armaColisores;
    public FaixaNome faixa;
    public Faixa faixaValores;

    [HideInInspector] public Material materialTesteColisores;
    int xp;

    [Range(0,10)]
    [SerializeField] public int forca;
    [Range(0, 10)]
    [SerializeField] public int agilidade;
    [HideInInspector] public AnimatorStateMachine[] estadoCombateStates;
    [HideInInspector] public AnimatorStateMachine[] estadoCombateAtaques;

    public float[][] cooldownSkills; //array de armas e dentro de cada uma dessas armas tem 3 skills.
    protected float[][] cooldownSkillsBaseTime;


    void Start()
    {
        Iniciar();
    }

    protected void Iniciar()
    {
        vida = vidaMax;
        vidaVigor = vidaMax;

        AtualizarAtributosSet(forca, agilidade);
        AtualizarArma(arma);
        AtualizarTalisma(talisma);
        AtualizarFaixa(faixa);

        PreencherAnimator(false);
        PreencherColisoresValoresDano();

        cooldownSkillsBaseTime = new float[Combate.Instance.tipoArmasDicionario.Count][];

        for (int i = 0; i < cooldownSkillsBaseTime.Length; i++)
        {
            cooldownSkillsBaseTime[i] = new float[Combate.Instance.tipoArmasDicionario[i].golpeSkills.Length];

            for (int j = 0; j < cooldownSkillsBaseTime[i].Length; j++)
            {
                cooldownSkillsBaseTime[i][j] = Combate.Instance.tipoArmasDicionario[i].golpeSkills[j].cooldown;
            }
        }

        cooldownSkills = new float[Combate.Instance.tipoArmasDicionario.Count][];

        for (int i = 0; i < cooldownSkills.Length; i++)
        {
            cooldownSkills[i] = new float[Combate.Instance.tipoArmasDicionario[i].golpeSkills.Length];

            for (int j = 0; j < cooldownSkills[i].Length; j++)

            {
                cooldownSkills[i][j] = Combate.Instance.tipoArmasDicionario[i].golpeSkills[j].cooldown;
            }
        }

    }

    public virtual void AtualizarAtributosAdicionar(int forcaMais, int agilidadeMais)
    {
        forca = Mathf.Clamp(forca += forcaMais, 0, 10);
        agilidade = Mathf.Clamp(agilidade += agilidadeMais, 0, 10);
    }
    public virtual void AtualizarAtributosSet(int forcaMais, int agilidadeMais)
    {
        forca = Mathf.Clamp(forcaMais, 0, 10);
        agilidade = Mathf.Clamp(agilidadeMais, 0, 10);
    }

    public virtual void AtualizarArma(ArmaNome armaNova)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }
        arma = armaNova;
        armaValores = Combate.Instance.armasDicionario[(int)armaNova];
    }
    public virtual void AtualizarArma(Arma armaNova)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }

        arma = (ArmaNome)armaNova.ID;
        armaValores = armaNova;
    }

    public virtual void AtualizarFaixa(FaixaNome faixaNova)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }
        faixa = faixaNova;
        faixaValores = Combate.Instance.faixasDicionario[(int)faixaNova];
    }
    public virtual void AtualizarFaixa(Faixa faixaNova)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }

        faixa = (FaixaNome)faixaNova.ID;
        faixaValores = faixaNova;
    }

    public virtual void AtualizarTalisma(TalismaNome talismaNovo)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }
        talisma = talismaNovo;
        talismaValores = Combate.Instance.talismaDicionario[(int)talismaNovo];
    }
    public virtual void AtualizarTalisma(Talisma talismaNovo)
    {
        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }

        talisma = (TalismaNome)talismaNovo.ID;
        talismaValores = talismaNovo;
    }



    #region Combate
    protected void AtaqueSimples()
    {
        if (anim.GetBool("emCombate")) //botao de ataque so funciona se tiver em combate
        {
            if (anim.GetBool("atacando"))
                return;

            anim.SetBool("atacando", true);
            anim.SetBool("emCombate", true);

        } //ataque
    }
    protected void AtaqueSkill(int num)
    {
        num -= 1;
        if (anim.GetBool("emCombate") && cooldownSkills[(int)arma][num] >= cooldownSkillsBaseTime[(int)arma][num])
        {
            if (anim.GetBool("atacando") && anim.GetBool("atacandoSkill"))
                return;

            anim.SetBool("atacando", true);
            anim.SetBool("atacandoSkill", true);
            anim.SetBool("emCombate", true);
            anim.SetInteger("ataqueSkill", num);

            cooldownSkills[(int)arma][num] = 0; //zerar o tempo de cooldown, pq ele acabou de usar
        } //ataque skill

    }
    public void ComboGolpeEncerrarAgora()
    {
        anim.SetBool("atacando", false);
    }
    public void GolpeComboInicio()
    {
        anim.SetBool("atacando", false);
    }
    public void GolpeSkillEncerrar()
    {
        anim.SetBool("atacandoSkill", false);
    }

    public void RootMotionLigar()
    {
        PlayerController.Instance.GetComponent<PlayerController>().anim.applyRootMotion = true;
    }
    public void RootMotionDesligar()
    {
        PlayerController.Instance.GetComponent<PlayerController>().anim.applyRootMotion = false;
    }

    #endregion

    public void PrintVariables()
    {
        Debug.Log("emCombate " + PlayerController.Instance.anim.GetBool("emCombate"));
        Debug.Log("atacando " + PlayerController.Instance.anim.GetBool("atacando"));
        Debug.Log("ataque " + PlayerController.Instance.anim.GetBool("ataque"));
        Debug.Log("atacandoSkill " + PlayerController.Instance.anim.GetBool("atacandoSkill"));
        Debug.Log("ataqueSkill " + PlayerController.Instance.anim.GetBool("ataqueSkill"));
    }
    public void RecuperarSkillsCountdown()
    {
        for (int i = 0; i < cooldownSkills.Length; i++)
        {
            for (int j = 0; j < cooldownSkills[i].Length; j++)
            {
                if (cooldownSkills[i][j] >= cooldownSkillsBaseTime[i][j]) //se ja tiver no maximo a skill, segue o baile
                {
                    cooldownSkills[i][j] = cooldownSkillsBaseTime[i][j];
                }
                else //se nao tiver recuperado o cooldown todo ainda, continua recuperando
                {
                    float parcelaRecuperada = 0.02f;
                    cooldownSkills[i][j] += parcelaRecuperada;
                }
            }
        }
    }
    public void PreencherAnimator(bool debug)
    {
        anim = GetComponent<Animator>();
        var runtimeController = anim.runtimeAnimatorController;

        if (runtimeController == null)
        {
            Debug.LogError("ERRO DE COMBATE. OBJ " + gameObject.name + "\t" + "O animator está sem controller");
            return;
        }

        var controller = UnityEditor.AssetDatabase.LoadAssetAtPath<UnityEditor.Animations.AnimatorController>(UnityEditor.AssetDatabase.GetAssetPath(runtimeController));

        if (controller.layers[0].stateMachine.entryTransitions.Length < 2)
        {
            Debug.LogError("ERRO DE COMBATE. OBJ " + gameObject.name + "\t" + "O animator deveria ter pelo menos 2 transicoes saídas do Entry.");
            return;
        }

        var stateMachine = controller.layers[0].stateMachine.entryTransitions[1].destinationStateMachine;

        if (stateMachine == null)
        {
            Debug.LogError("ERRO DE COMBATE. OBJ " + gameObject.name + "\t" + "O animator detoveria estar com a 2 transição vinda do Entry indo para um stateMachine de combate.");
            return;
        }

        if (stateMachine.entryTransitions.Length < 3)
        {
            Debug.LogError("ERRO DE COMBATE. OBJ " + gameObject.name + "\t" + "O stateMachine de combate deveria ter 3 transicoes do Entry na ordem: idle, ataque, esquiva.");
            return;
        }

        estadoCombateStates = new AnimatorStateMachine[3]; // 0 é idle, 1 é ataque, 2 é esquiva

        for (int i = 0; i < 3; i++)
        {
            estadoCombateStates[i] = stateMachine.entryTransitions[i].destinationStateMachine;
        }

        estadoCombateAtaques = new AnimatorStateMachine[estadoCombateStates[1].entryTransitions.Length];

        for (int i = 0; i < estadoCombateStates[1].entryTransitions.Length; i++)
        {
            estadoCombateAtaques[i] = estadoCombateStates[1].entryTransitions[i].destinationStateMachine;
        }

       
        if (debug)
            DebugAtaques();
    }

    void DebugAtaques()
    {
        var atkStateMachine = estadoCombateStates[1];
        var tipoArmasStateMachine = atkStateMachine.entryTransitions;

        if (Combate.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO DE ARMAS PRIMEIRO");
            return;
        }
        var tipoArmasDic = Combate.Instance.tipoArmasDicionario;


        //erro de length
        if (tipoArmasStateMachine.Length != tipoArmasDic.Count)
        {
            Debug.LogError("ERRO COMBATE" + " diferença no número de tipo de armas: " + "\t" + "No animator são " + tipoArmasStateMachine.Length + "No dicionario são " + tipoArmasDic.Count);
            return;
        }

        Debug.Log("TIPO DE ARMAS: COMPARAÇÃO DICIONARIO / ANIMATOR");

        for (int i = 0; i < tipoArmasDic.Count; i++)
        {
            Debug.Log("[" + i + "] " + tipoArmasDic[i].nome + " / " + tipoArmasStateMachine[i].destinationStateMachine);
        }


        //erro de length
        int numMaior = 0;
        if (tipoArmasDic.Count > tipoArmasStateMachine.Length)
            numMaior = tipoArmasDic.Count;
        else
            numMaior = tipoArmasStateMachine.Length;

        for (int i = 0; i < numMaior; i++)
        {
            var tipoDic = tipoArmasDic[i];
            var tipoAnim = tipoArmasStateMachine[i].destinationStateMachine;

            if ((tipoDic.golpeCombo.Length + tipoDic.golpeSkills.Length) != tipoAnim.entryTransitions[0].destinationState.transitions.Length)
            {
                    Debug.LogError("ERRO COMBATE" + " diferença no número de combos: " + "\t" + "[" + tipoDic.nome + "] " + "No animator são " + tipoAnim.entryTransitions[0].destinationState.transitions.Length + " e no dicionario são " + tipoDic.golpeCombo.Length + tipoDic.golpeSkills.Length);
                    return;
            } //check diferenca no num de combos+num skills comparado ao num transicoes no animator

            for (int j = 0; j < tipoDic.golpeCombo.Length; j++)
            {
                var numGolpesNoComboDic = tipoDic.golpeCombo[j].sequenciaGolpes.Length;
                int numTentativas = numGolpesNoComboDic;
                var comboStart = tipoAnim.entryTransitions[0].destinationState.transitions[j].destinationState;
                var comboPart = comboStart;

                if (numGolpesNoComboDic == 0)
                {
                    Debug.LogError("ARMA " + tipoDic.nome + " COMBO " + tipoDic.golpeCombo[j].nome + " NÃO TEM GOLPES");
                    return;
                }

                for (int k = 0; k < numTentativas; k++)
                {
                    print(numTentativas);
                    if (k != numTentativas-1)
                    {
                        if (comboPart.transitions.Length == 2) //continuar combo, ir para skill ou sair do combo 
                        {
                            Debug.LogError("ERRO COMBATE" + " diferença no número de golpes da arma: " + "[" + tipoDic.nome + "] COMBO " + j + " [" + tipoDic.golpeCombo[j] + "]" + " > CHECAR NO DICIONARIO E NO ANIMATOR");
                            return;
                        }
                        comboPart = comboPart.transitions[0].destinationState;
                    }
                    else
                        if(comboPart.transitions.Length > 3) //ir para skill ou sair do combo
                        {
                            Debug.LogError("ERRO COMBATE" + " diferença no número de golpes da arma: " + "[" + tipoDic.nome + "] COMBO " + j + " [" + tipoDic.golpeCombo[j] + "]" + " > CHECAR NO DICIONARIO E NO ANIMATOR");
                            return;
                        }
                }

            }
        }

        Debug.Log("COMBOS: COMPARAÇÃO DICIONARIO / ANIMATOR");
        for (int i = 0; i < tipoArmasDic.Count; i++)
        {
            var tipo = tipoArmasDic[i];
            var anim = tipoArmasStateMachine[i];
            Debug.Log("[" + i + "] " + tipo.nome);
            for (int j = 0; j < tipo.golpeCombo.Length; j++)
            {
                var comboDic = tipo.golpeCombo[j];
                var comboAnim = anim.destinationStateMachine.entryTransitions[0].destinationState.transitions[j].destinationState;
                Debug.Log("[" + tipo.nome + "] " + comboDic.nome + " / " + comboAnim.name + " [COMBO NUM " + j + "]");
            }
        }

    }

    public void PreencherColisoresValoresDano()
    {
        if (armaColisores == null)
        {
            PreencherColisoresNomes();
        }

        for (int i = 0; i < Combate.Instance.armasDicionario.Count; i++)
        {
            Collider[] colArray = armaColisores.colliders[i].collidersArma;


            for (int j = 0; j < colArray.Length; j++)
            {
                GameObject colisorObj = colArray[j].gameObject;
                if (colArray.Length == 0) break;
                if (!colisorObj) Debug.LogError("Objeto: " + gameObject + ". O item número " + j + " do array de colisores da arma " + armaColisores.colliders[i].nome + " está vazio.");
                if (!colisorObj.GetComponent<ColisorDano>()) Debug.LogError("Objeto: " + gameObject + ". O item número " + j + " do array de colisores da arma " + armaColisores.colliders[i].nome + " não tem o script de ColisorDano.");
                //colisorObj.AddComponent<colisorAtaque>();

                ColisorDano colisor = colisorObj.GetComponent<ColisorDano>();
                colisor.gameObject.GetComponent<Collider>().enabled = false;
                colisor.combatenteOrigem = this;
                colisor.dano = Combate.Instance.armasDicionario[i].dano;

                if (materialTesteColisores)
                colisor.gameObject.GetComponent<MeshRenderer>().material = materialTesteColisores;
            }
        }
    }
    public void PreencherColisoresNomes()
    {
        ColliderArma[] collidersBase = ArmaCollidersValues.colliders;
        armaColisores.colliders = new ColliderArma[collidersBase.Length];

        for (int i = 0; i < collidersBase.Length; i++)
        {
            string nomeArma = ArmaCollidersValues.colliders[i].nome;
            //print("[" + i + "] " + nomeArma);
            armaColisores.colliders[i] = new ColliderArma();
            armaColisores.colliders[i].nome = nomeArma;
        }
    }
    public void SortearCombo()
    {
        int comboEscolhido = Random.Range(0, Combate.Instance.tipoArmasDicionario[(int)arma].golpeCombo.Length);
        anim.SetInteger("ataque", comboEscolhido);
    }
    public void SortearSkill()
    {
        int skillEscolhida = Random.Range(0, Combate.Instance.tipoArmasDicionario[(int)arma].golpeSkills.Length);
        anim.SetInteger("ataqueSkill", skillEscolhida);
    }

    #region Colisores Ataque
    public void ColisorArmaTodosOn()
    {
        Collider[] armaAtual = armaColisores.colliders[(int)arma].collidersArma;
        for (int i = 0; i < armaAtual.Length; i++)
        {
            armaAtual[i].GetComponent<Collider>().enabled = true;
            armaAtual[i].GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }
    public void ColisorArmaTodosOff()
    {
        Collider[] armaAtual = armaColisores.colliders[(int)arma].collidersArma;
        for (int i = 0; i < armaAtual.Length; i++)
        {
            armaAtual[i].GetComponent<Collider>().enabled = false;
            armaAtual[i].GetComponent<MeshRenderer>().material.color = Color.white;
        }
    }
    public void ColisorArmaOn(int numColisor)
    {
        Collider[] armaAtual = armaColisores.colliders[(int)arma].collidersArma;
        armaAtual[numColisor].GetComponent<Collider>().enabled = true;
        armaAtual[numColisor].GetComponent<MeshRenderer>().material.color = Color.red;
    }
    public void ColisorArmaOff(int numColisor)
    {
        Collider[] armaAtual = armaColisores.colliders[(int)arma].collidersArma;
        armaAtual[numColisor].GetComponent<Collider>().enabled = false;
        armaAtual[numColisor].GetComponent<MeshRenderer>().material.color = Color.white;
    }
    public void MostrarEsconderColisores()
    {
        for (int i = 0; i < armaColisores.colliders.Length; i++)
        {
            for (int j = 0; j < armaColisores.colliders[i].collidersArma.Length; j++)
            {
                MeshRenderer meshR = armaColisores.colliders[i].collidersArma[j].gameObject.GetComponent<MeshRenderer>();
                meshR.enabled = (!meshR.enabled);
            }
        }
    } //mesh renderer
    public void MostrarColisores()
    {
        for (int i = 0; i < armaColisores.colliders.Length; i++)
        {
            for (int j = 0; j < armaColisores.colliders[i].collidersArma.Length; j++)
            {
                MeshRenderer meshR = armaColisores.colliders[i].collidersArma[j].gameObject.GetComponent<MeshRenderer>();
                meshR.enabled = true;
            }
        }
    } //mesh renderer
    #endregion
}

public class Humanoide : Combatente
{
    string nome;
    int desobediencia;

}

public class HumanoidePacifico : MonoBehaviour
{
    string nome;
    Faixa faixa;
    int desobediencia;
}