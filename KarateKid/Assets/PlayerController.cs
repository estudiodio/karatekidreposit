﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Rewired;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : Humanoide
{
    public static PlayerController Instance;

    [HideInInspector] public Image[] imgsCooldownSkills;

    [Space(10)]
    #region Fisica
    public GameObject Cam;
    public GameObject LocalN;
    public GameObject LocalC;

    Vector3 direcaoDash;

    public float rotationSpeed = 180;
    public Transform inimigo;
    public Transform target;
    public float targetspeed = 1;

    public float speed;

    [SerializeField] bool noChao;
    [SerializeField] bool quaseNoChao; // pra animacao de "pulo > andar" mais bonito
    bool temParede;
    bool dandoDash;
    [SerializeField] bool podeDash = true; // "cooldown"

    bool mirando;
    public bool batendo;

    public float DashForce = 8;

    public float lineRange = 0.15f;
    public float lineRangeMaior = 0.3f;
    public float JumpForce = 8;
    public float speedJump = 1.5f;
    public float runningSpeed = 6;
    public float walkingSpeed = 3;
    public RaycastHit hit;
    #endregion

    #region Rewired - Controle de Inputs 
    //private Player playerRewired;
    public int playerID;
    #endregion


    Rigidbody rb;


    void Start()
    {
        Instance = this;
        //imortal = true;

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        speed = walkingSpeed;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Iniciar();

       // playerRewired = ReInput.players.GetPlayer(playerID);
    }

    #region Atualizar Status do Player
    public override void AtualizarAtributosAdicionar(int forcaMais, int agilidadeMais)
    {
        base.AtualizarAtributosAdicionar(forcaMais, agilidadeMais);
        CanvasManager.Instance.inventario.UpdatePlayer();
    }
    public override void AtualizarAtributosSet(int forcaMais, int agilidadeMais)
    {
        base.AtualizarAtributosSet(forcaMais, agilidadeMais);
        CanvasManager.Instance.inventario.UpdatePlayer();
    }

    public override void AtualizarArma(ArmaNome armaNova)
    {
        base.AtualizarArma(armaNova);
        CanvasManager.Instance.inventario.UpdateArma();
        CanvasManager.Instance.pause.UpdateArma();
    }
    public override void AtualizarArma(Arma armaNova)
    {
        base.AtualizarArma(armaNova);
        CanvasManager.Instance.inventario.UpdateArma();
        CanvasManager.Instance.pause.UpdateArma();
    }

    public override void AtualizarFaixa(FaixaNome faixaNova)
    {
        base.AtualizarFaixa(faixaNova);
        CanvasManager.Instance.inventario.UpdateFaixa();
        CanvasManager.Instance.pause.UpdateFaixa();
    }
    public override void AtualizarFaixa(Faixa faixaNova)
    {
        base.AtualizarFaixa(faixaNova);
        CanvasManager.Instance.inventario.UpdateArma();
        CanvasManager.Instance.pause.UpdateFaixa();
    }

    public override void AtualizarTalisma(TalismaNome talismaNovo)
    {
        base.AtualizarTalisma(talismaNovo);
        CanvasManager.Instance.inventario.UpdateTalisma();
        CanvasManager.Instance.pause.UpdateTalisma();
    }
    public override void AtualizarTalisma(Talisma talismaNovo)
    {
        base.AtualizarTalisma(talismaNovo);
        CanvasManager.Instance.inventario.UpdateTalisma();
        CanvasManager.Instance.pause.UpdateTalisma();
    }
    #endregion

    void Update()
    {
        if (!vivo) return;

        #region Inputs Movimentação e Combate

        //Debug.DrawRay(transform.position, -Vector3.up * lineRange, Color.green, .1f);
        if (checarChao)
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, lineRange, 1 << 11, QueryTriggerInteraction.Ignore))
        {
            noChao = true;
            //Debug.Log("CHAO " + hit.transform.gameObject);
        }
        //Debug.DrawRay(transform.position, -Vector3.up * lineRangeMaior, Color.red, .1f);
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, lineRange + 1, 1 << 11, QueryTriggerInteraction.Ignore))
        {
            quaseNoChao = true;
            //Debug.Log("QUASE " + hit.transform.gameObject);
        }
        else quaseNoChao = false;

        #region Check if No Chao -Milena mudou, dsclp vittor. 03/11
        /*
        if (Physics.SphereCast(new Vector3(transform.position.x, transform.position.y + 0.13f, transform.position.z), lineRange, new Vector3(0, 0, 0), out hit, 1 << 11))
        {
            noChao = false;
            Debug.Log("CHAO " + hit.collider.gameObject.name);
        }
        if (Physics.SphereCast(new Vector3(transform.position.x, transform.position.y - 0.4f, transform.position.z), lineRange, new Vector3(0, 0, 0), out hit, 1 << 11))
        {
            quaseNoChao = true;
            Debug.Log("QUASE " + hit.collider.gameObject.name);
        }
        else quaseNoChao = false;*/
        #endregion

        temParede = Physics.Raycast(transform.position, Vector3.forward, 2); // Verificar se tá encostando na parede.


        if (Input.GetKey(KeyCode.LeftShift)) Correr(); // Correr. //playerRewired.GetButton("Correr"))
        if (Input.GetKeyUp(KeyCode.LeftShift)) speed = walkingSpeed * agilidade; // Parar de Correr.


        if (Input.GetButtonDown("Jump") && noChao && !dandoDash ) Pular();

        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !temParede && noChao && !batendo)// Suavizar parada
        {
            transform.position += transform.forward * Input.GetAxis("Vertical") * 5 * Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            mirando = !mirando; // Alterna o estado de mirando.
        }

        if (noChao && !dandoDash && !mirando)//Rotação 
        {
                transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime); // Rotacionar.
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && !noChao) Travar(); // Segura o perssonagem no ar.
            
        if (Input.GetKeyDown(KeyCode.Mouse1) && !noChao) Destravar(); // Libera o perssonagem do ar.


        if (mirando && inimigo != null)
        {
            target.transform.position = Vector3.Lerp(target.transform.position, inimigo.transform.position, 1);
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z)); // Travar no alvo.

            Cam.transform.position = Vector3.Lerp(Cam.transform.position, LocalC.transform.position, 8  * Time.deltaTime);
        }

        if (!mirando) Cam.transform.position = Vector3.Lerp(Cam.transform.position, LocalN.transform.position, targetspeed * Time.deltaTime);

        if (noChao && !dandoDash && !batendo) Dash();



        #endregion

        if (anim.GetBool("emCombate"))
        {
            if (Input.GetKey(KeyCode.Mouse0)) //ataque simples
                AtaqueSimples();

            if (Input.GetKeyDown(KeyCode.Q))//playerRewired.GetButtonDown("Skill 1"))//ataque skill 1
                AtaqueSkill(1);

            if (Input.GetKeyDown(KeyCode.E))//playerRewired.GetButtonDown("Skill 2"))//ataque skill 2
                AtaqueSkill(2);

            if (Input.GetKeyDown(KeyCode.R))//playerRewired.GetButtonDown("Skill 3"))//ataque skill 3
                AtaqueSkill(3);
        } // inputs de combate

        if (Input.GetKeyDown(KeyCode.C))//playerRewired.GetButtonDown("ModoCombate")) //ligar/desligar modo combate
        {
            anim.SetBool("emCombate", !anim.GetBool("emCombate"));
            if (!anim.GetBool("emCombate"))
            {
                ComboGolpeEncerrarAgora();
            }

            // anim.applyRootMotion = !anim.applyRootMotion;  // Vitto 5/10
        } // entrar em modo combate
        
        Animator();

        if (Input.GetKeyDown(KeyCode.Return)) //TESTE PARA TOMAR DANO
            ReceberDano(10);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //StartCoroutine(LockCursor());
            GameObject pause = CanvasManager.Instance.pause.gameObject;
            pause.SetActive(!pause.activeSelf);
            if (pause.activeSelf)
            {
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

        } // DAR PAUSE

        if (Input.GetKeyDown(KeyCode.I))
        {
            //StartCoroutine(LockCursor());
            CanvasManager.Instance.inventario.gameObject.SetActive(!CanvasManager.Instance.inventario.gameObject.activeSelf);
        } // mostrar inventario

        Cam.transform.LookAt(new Vector3(transform.position.x + 0.2f, transform.position.y + 1, transform.position.z)); // Travar no alvo.
    }
    void FixedUpdate()
    {
        if (!vivo) return;

        RecuperarVida();

        Vector3 direction = Vector3.zero;
        direction = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical"); // Setar direção mirando

        Vector3 chao = direction * speed * Time.deltaTime * 50;
        if (noChao && !dandoDash && !batendo) rb.velocity = new Vector3(chao.x, rb.velocity.y, chao.z); // Andar
        //if (noChao && !dandoDash && !batendo) rb.velocity = chao; // Andar
        Vector3 semchao = direction * speed * Time.deltaTime * 25;
        if (!noChao && !dandoDash && !batendo) rb.velocity = new Vector3 (semchao.x, rb.velocity.y, semchao.z); // Andar

        if (dandoDash)
        {
            Vector3 forca = direcaoDash * DashForce * Time.deltaTime;
            //Vector3 forca = direcaoDash * DashForce * 0.06f;
            //Vector3 forca = new Vector3(Mathf.Clamp(direcaoDash.x,-.3f,.3f), Mathf.Clamp(direcaoDash.y, -.3f, .3f), Mathf.Clamp(direcaoDash.z, -.3f, .3f) * DashForce * Time.deltaTime);
            rb.AddForce(forca);

        }
        //if (dandoDash) transform.position += direcaoDash * 3 * DashForce * Time.deltaTime; // Dash, FashForce estava 18

        RecuperarSkillsCountdown();
        RecuperarSkillsCountdownImages();
    }

    IEnumerator LockCursor()
    {
        yield return new WaitForSeconds(.1f);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Pular()
    {
        rb.velocity = new Vector3(rb.velocity.x, JumpForce, rb.velocity.z);
        anim.SetTrigger("pular");
        noChao = false;
        checarChao = false;
        StartCoroutine("PuloIniciado");
    }
    void Correr()
    {
        speed = runningSpeed * agilidade;
    }
    void Dash() // Setar direção do Dash.
    {
        if (Input.GetKeyDown(KeyCode.LeftControl) && podeDash)
        {
            dandoDash = true;
            direcaoDash = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");
            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
                    direcaoDash = transform.forward;
             StartCoroutine("DashIniciado");
            StartCoroutine("DashCooldown");
        }
    }
    public void FimDash() // Finalizar o Dash.
    {
        dandoDash = false;
    }
    void Travar() // Segura o perssonagem no ar.
    {
        rb.drag = 100;
    }
    void Destravar()// Libera o perssonagem do ar.
    {
        rb.drag = 0.5f;
        rb.velocity = new Vector3(rb.velocity.x, -1, rb.velocity.z);
    }

    public void SetBatendo(bool truefalse)
    {
        PlayerController.Instance.GetComponent<PlayerController>().batendo = truefalse;
    }
    void Animator()
    {
        if (!batendo)
        {
            anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
            anim.SetFloat("Vertical", Input.GetAxis("Vertical"));
        }
        anim.SetBool("mirando", mirando);
        anim.SetBool("noChao", noChao);
        if (checarChao) anim.SetBool("quaseNoChao", quaseNoChao); else anim.SetBool("quaseNoChao", false);
        anim.SetBool("dash", dandoDash);
        anim.SetBool("batendo", batendo);
        anim.SetBool("correndo", Input.GetKey(KeyCode.LeftShift));

    }
    public void RecuperarSkillsCountdownImages()
    {
        for (int j = 0; j < cooldownSkills[(int)arma].Length; j++)
        {
            imgsCooldownSkills[j].fillAmount = cooldownSkills[(int)arma][j] / cooldownSkillsBaseTime[(int)arma][j];
        }
    }

    private void OnTriggerStay(Collider other)
    {
        //noChao = Physics.Raycast(transform.position, -Vector3.up, lineRange);
        rb.drag = 0.5f;
    }

    IEnumerator DashIniciado()
    {    
        podeDash = false;
        yield return new WaitForSeconds(0.3f);//estava em 0.05
        FimDash();
    }

    IEnumerator DashCooldown() //milena 03.11
    {
        yield return new WaitForSeconds(.7f);
        podeDash = true;
    }

    bool checarChao = true;
    IEnumerator PuloIniciado()//milena 03.11
    {
        yield return new WaitForSeconds(0.3f);
        checarChao = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta;
        //Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y + 0.13f, transform.position.z) + new Vector3(0, 0, 0), lineRange);
        //Gizmos.DrawSphere(new Vector3(transform.position.x, transform.position.y - 0.4f, transform.position.z) + new Vector3(0, 0, 0), lineRange);
    }

}

#region Nao vou apagar pq talvez eu precise um dia
[System.Serializable]
public class animTrigger
{
    public string nome;
    public bool var;
    public Animator anim;

    public void Trigger(bool value)
    {
        if (value)
        {
            var = true;
            anim.SetTrigger(nome);
            var = false;
        }
        else
        {
            var = false;
            anim.ResetTrigger(nome);
        }
    }
}

[System.Serializable]
public class animFloat
{
    public string nome;
    public float var;
    public Animator anim;

    public void Set(float value)
    {
        var = value;
        anim.SetFloat(nome, var);
    }
}

[System.Serializable]
public class animBool
{
    public string nome;
    public bool var;
    public Animator anim;

    public void Set(bool value)
    {
        var = value;
        anim.SetBool(nome, var);
    }
}
#endregion