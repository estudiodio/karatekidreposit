﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class Movimenta_cao2 : MonoBehaviour
{
    public float rotationSpeed = 180;
    public Transform target;

    public float speed;

    public bool noChao;//***************************
    public bool temParede;
    public bool dandoDash;
    public bool mirando;

    public float DashForce = 8;

    public float lineRange = 0.15f;
    public float JumpForce = 8;
    public float speedJump = 1.5f;
    public float runningSpeed;
    public float walkingSpeed = 8;

    public GameObject CamC;
    public GameObject CamN;

    Vector3 direcaoDash;

    Rigidbody rb;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        speed = walkingSpeed;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Physics.Raycast(transform.position, -Vector3.up, lineRange))
            noChao = false; // Verificar se tá no chão.

        temParede = Physics.Raycast(transform.position, Vector3.forward, 2); // Verificar se tá encostando na parede.

        if (mirando)
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z)); // Travar no alvo.
           

        if (Input.GetKey(KeyCode.LeftShift))
            Correr(); // Correr.
        if (Input.GetKeyUp(KeyCode.LeftShift))
            speed = walkingSpeed; // Parar de Correr.

        if (noChao && !dandoDash && !mirando)//Rotação 
        {
            if (!Input.GetKey(KeyCode.S)) transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime); // Rotacionar.
            if (Input.GetKey(KeyCode.S))  transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * rotationSpeed * Time.deltaTime); // Rotacionar.          
        }



        if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !temParede && noChao)// Suavizar parada
            transform.position += transform.forward * Input.GetAxis("Vertical") * 5 * Time.deltaTime;

        if (!noChao && !temParede && Mathf.Abs(rb.velocity.magnitude) < 3 && rb.drag == 0)
            transform.position += transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * speedJump; // Adiciona Velocidade no ar.


        if (Input.GetKeyDown(KeyCode.T))
        {
            mirando = !mirando; // Alterna o estado de mirando.
            CamC.SetActive(!mirando);
            CamN.SetActive(mirando);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && !noChao)
            Travar(); // Segura o perssonagem no ar.
        if (Input.GetKeyDown(KeyCode.Mouse1) && !noChao)
            Destravar(); // Libera o perssonagem do ar.



        if (noChao && !dandoDash) Dash(); 
        Animator();
       
    }

    private void FixedUpdate()
    {
        Vector3 direction = Vector3.zero;
       /* if (!mirando) direction = transform.forward * Input.GetAxis("Vertical"); // Setar direção sem mirar
        if (mirando)*/ direction = transform.right * Input.GetAxis("Horizontal") +  transform.forward * Input.GetAxis("Vertical"); // Setar direção mirando

        if (noChao && !dandoDash) rb.velocity = direction * speed * Time.deltaTime * 50; // Andar

        if (dandoDash) transform.position += direcaoDash * 1 * DashForce * Time.deltaTime; // Dash
    }

    void Pular()
    {
            rb.velocity = new Vector3(rb.velocity.x, JumpForce, rb.velocity.z);
            anim.SetTrigger("Pular");
            noChao = false;
           // anim.applyRootMotion = false;
    }

    void Correr()
    {
        speed = runningSpeed;
    }

    void Dash() // Setar direção do Dash.
    {

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            dandoDash = true;
            direcaoDash = transform.right * Input.GetAxis("Horizontal") + transform.forward * Input.GetAxis("Vertical");
            if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0) direcaoDash = transform.forward * 1;
        }
    }

    public void FimDash() // Finalizar o Dash.
    {
        dandoDash = false;
    }

    void Travar () // Segura o perssonagem no ar.
    {
        rb.drag = 100;
        //anim.applyRootMotion = true;

    }

    void Destravar()// Libera o perssonagem do ar.
    {
        rb.drag = 0;
        rb.velocity = new Vector3(rb.velocity.x, -1, rb.velocity.z);
    }


    private void OnTriggerStay(Collider other)
    {
        noChao = Physics.Raycast(transform.position, -Vector3.up, lineRange);
        rb.drag = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        //anim.applyRootMotion = true;
    }

    void Animator()
    {
        anim.SetFloat("Horizontal", Input.GetAxis("Horizontal"));
        anim.SetFloat("Vertical", Input.GetAxis("Vertical"));

        anim.SetBool("Mirando", mirando);
        anim.SetBool("noChao", noChao);
        anim.SetBool("DandoDash", dandoDash);
        anim.SetBool("Run", Input.GetKey(KeyCode.LeftShift));
    }
}
