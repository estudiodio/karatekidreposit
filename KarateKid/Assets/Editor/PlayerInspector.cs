﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerController))]
//[CanEditMultipleObjects]
public class PlayerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        PlayerController targ = target as PlayerController;

        GUILayout.Space(20);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("INSPECTOR TESTE", EditorStyles.boldLabel);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        if (GUILayout.Button("VER VALOR ARMA"))
        {
            targ.AtualizarArma(targ.arma);
        }

        if (GUILayout.Button("DEBUG CHECK ANIMATOR"))
        {
            targ.PreencherAnimator(true);
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("**RESET ARRAY ARMA COLLIDERS**"))
        {
            targ.PreencherColisoresNomes();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("MOSTRAR/ESCONDER COLISORES"))
        {
            targ.MostrarEsconderColisores();
        }
        if (GUILayout.Button("MOSTRAR COLISORES"))
        {
            targ.MostrarColisores();
        }
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        if (GUILayout.Button("RECEBER DANO"))
        {
            targ.ReceberDano(15);
        }
        GUILayout.EndHorizontal();

        GUILayout.Space(30);
        DrawDefaultInspector();

    }
}