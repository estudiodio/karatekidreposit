﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Humanoide))]
public class HumanoideInspector : Editor
{
    public override void OnInspectorGUI()
    {
        Humanoide targ = target as Humanoide;

        GUILayout.Space(20);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label("INSPECTOR TESTE", EditorStyles.boldLabel);
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        if (GUILayout.Button("VER VALOR ARMA"))
        {
            targ.AtualizarArma(targ.arma);
        }

        if (GUILayout.Button("CHECK ANIMATOR"))
        {
            targ.PreencherAnimator(true);
        }
        if (GUILayout.Button("ARRAY ARMA COLLIDERS"))
        {
            targ.PreencherColisoresNomes();
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("MOSTRAR/ESCONDER COLISORES"))
        {
            targ.MostrarEsconderColisores();
        }
        if (GUILayout.Button("MOSTRAR COLISORES"))
        {
            targ.MostrarColisores();
        }
        GUILayout.Space(20);

        GUILayout.Space(30);
        DrawDefaultInspector();

    }
}



